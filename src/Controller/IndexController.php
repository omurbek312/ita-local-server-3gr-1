<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use \Curl\Curl;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class IndexController extends Controller
{
    /**
     * @Route("/",name ="app_index")
     * @throws \ErrorException
     */
    public function indexAction()
    {
        $curl = new Curl();
        $curl->get('http://127.0.0.1:8001/ping?token' .
            '=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9' .
            '.eyJvcmdhbml6YXRpb25OYW1lIjoiVW1icmVsbGEiLCJvcmdhbml' .
            '6YXRpb25TaXRlVVJMIjoiMTIzLmNvbSIsImlkIjoxfQ' .
            '.uhUTBjJWK42lgC1lznt5ESQEVRemBdyu7O60hhPMHvo');
        if ($curl->error) {
            return new Response('error:' . $curl->errorCode . ';' . $curl->errorMessage . "\n");
        } else {
            return new Response(var_export($curl->response, true));
        }
    }

    /**
     * @Route("/register",name="register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user->setRole(['ROLE_USER']);

            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);

            $em->persist($user);
            $em->flush();
        }
        return $this->render('register.html.twig', ['form' => $form->createView()]);
    }
}