<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
     $builder->add('email',EmailType::class,['attr'=>['placeholder'=>'Введите почту']]);
     $builder->add('idPassport',TextType::class,['attr'=>['placeholder'=>'Введите номер пасспорта']]);
     $builder->add('password',TextType::class,['attr'=>['placeholder'=>'Введите пароль']]);
     $builder->add('add',SubmitType::class,['label'=>'Регистрация']);
    }
}